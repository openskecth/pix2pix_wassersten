# pix2pix_wasserstein
This code is [pix2pix](https://github.com/affinelayer/pix2pix-tensorflow) code, where the loss is changed to a wasserstein loss.


## Data

Testing data can be downloaded [here]().
Trained models for each of datasets can be download [here]().

## This code is a part of OpenSketch publication
Project page:
https://ns.inria.fr/d3/OpenSketch/

Additional data:
https://repo-sam.inria.fr/d3/OpenSketch/


## Contact
If you have any questions please contact Yulia Gryaditskaya yulia.gryaditskaya@gmail.com
If something is missing please contact yulia.gryaditskaya@gmail.com.